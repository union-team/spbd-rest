package com.spbd.rest.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.spbd.cashflow.spring.model.Diary;

public class CashflowJSON implements Serializable {

	private String id;

	private String name;

	private Date dateOfBirth;
	
	private String mobileId;

	private double loanAmount;

	private Date dateSubmitted;

	private double weeklyRepayment;

	private int weeksToPay;

	private String summary;

	private Boolean accepted;

	private int numberOfDiaryMonths;

	private double diaryBusinessInAverage;

	private double diaryBusinessOutAverage;

	private double diaryBusinessNetAverage;

	private double diaryHouseholdInAverage;

	private double diaryHouseholdOutAverage;

	private double diaryHouseholdNetAverage;

	private double expectedBusinessInAverage;

	private double expectedBusinessOutAverage;

	private double expectedBusinessNetAverage;

	private double expectedHouseholdInAverage;

	private double expectedHouseholdOutAverage;

	private double expectedHouseholdNetAverage;

	// cashflow analysis 1
	private double netBusinessIncomeAsSalesRevenueFinancialDiary;

	private String netBusinessIncomeAsSalesRevenueFinancialDiaryRisk;

	// cashflow analysis 2
	private double netBusinessIncomeAsSalesRevenueBudget;

	private String netBusinessIncomeAsSalesRevenueBudgetRisk;

	// cashflow analysis 3
	private double differenceBetweenPresentAndExpectedIncomeBusiness;

	private String differenceBetweenPresentAndExpectedIncomeBusinessRisk;

	// cashflow analysis 4
	private double loanRepaymentOfHouseholdIncomeFiancialDiary;

	private String loanRepaymentOfHouseholdIncomeFiancialDiaryRisk;

	// cashflow analysis 5
	private double loanRepaymentOfHouseholdIncomeBudget;

	private String loanRepaymentOfHouseholdIncomeBudgetRisk;

	// cashflow analysis 6
	private double differenceBetweenPresentAndExpectedHousehold;

	private String differenceBetweenPresentAndExpectedHouseholdRisk;

	// cashflow analysis 7
	private double householdExpensesOfHouseholdIncomeFinancialDiary;

	private String householdExpensesOfHouseholdIncomeFinancialDiaryRisk;

	// cashflow analysis 8
	private double householdExpensesOfHouseholdIncomeBudget;

	private String householdExpensesOfHouseholdIncomeBudgetRisk;

	private List<Diary> diaries;

	// attach user details before send gson
//	private UserDetails userDetails;

	private boolean uploaded;

	// private Applicant applicant;

	public CashflowJSON() {
		diaries = new ArrayList<Diary>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Diary> getDiaries() {
		return diaries;
	}

	public void setDiaries(List<Diary> diaries) {
		this.diaries = diaries;
	}

	public double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}

	// public Applicant getApplicant() {
	// return applicant;
	// }
	//
	// public void setApplicant(Applicant applicant) {
	// this.applicant = applicant;
	// }

	public Date getDateSubmitted() {
		return dateSubmitted;
	}

	public void setDateSubmitted(Date dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public double getWeeklyRepayment() {
		return weeklyRepayment;
	}

	public void setWeeklyRepayment(double weeklyRepayment) {
		this.weeklyRepayment = weeklyRepayment;
	}

	public int getWeeksToPay() {
		return weeksToPay;
	}

	public void setWeeksToPay(int weeksToPay) {
		this.weeksToPay = weeksToPay;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public int getNumberOfDiaryMonths() {
		return numberOfDiaryMonths;
	}

	public void setNumberOfDiaryMonths(int numberOfDiaryMonths) {
		this.numberOfDiaryMonths = numberOfDiaryMonths;
	}

	public double getDiaryBusinessInAverage() {
		return diaryBusinessInAverage;
	}

	public void setDiaryBusinessInAverage(double diaryBusinessInAverage) {
		this.diaryBusinessInAverage = diaryBusinessInAverage;
	}

	public double getDiaryBusinessOutAverage() {
		return diaryBusinessOutAverage;
	}

	public void setDiaryBusinessOutAverage(double diaryBusinessOutAverage) {
		this.diaryBusinessOutAverage = diaryBusinessOutAverage;
	}

	public double getDiaryBusinessNetAverage() {
		return diaryBusinessNetAverage;
	}

	public void setDiaryBusinessNetAverage(double diaryBusinessNetAverage) {
		this.diaryBusinessNetAverage = diaryBusinessNetAverage;
	}

	public String getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk() {
		return netBusinessIncomeAsSalesRevenueFinancialDiaryRisk;
	}

	public void setNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk(String netBusinessIncomeAsSalesRevenueFinancialDiaryRisk) {
		this.netBusinessIncomeAsSalesRevenueFinancialDiaryRisk = netBusinessIncomeAsSalesRevenueFinancialDiaryRisk;
	}

	public String getNetBusinessIncomeAsSalesRevenueBudgetRisk() {
		return netBusinessIncomeAsSalesRevenueBudgetRisk;
	}

	public void setNetBusinessIncomeAsSalesRevenueBudgetRisk(String netBusinessIncomeAsSalesRevenueBudgetRisk) {
		this.netBusinessIncomeAsSalesRevenueBudgetRisk = netBusinessIncomeAsSalesRevenueBudgetRisk;
	}

	public String getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk() {
		return differenceBetweenPresentAndExpectedIncomeBusinessRisk;
	}

	public void setDifferenceBetweenPresentAndExpectedIncomeBusinessRisk(String differenceBetweenPresentAndExpectedIncomeBusinessRisk) {
		this.differenceBetweenPresentAndExpectedIncomeBusinessRisk = differenceBetweenPresentAndExpectedIncomeBusinessRisk;
	}

	public String getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk() {
		return loanRepaymentOfHouseholdIncomeFiancialDiaryRisk;
	}

	public void setLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk(String loanRepaymentOfHouseholdIncomeFiancialDiaryRisk) {
		this.loanRepaymentOfHouseholdIncomeFiancialDiaryRisk = loanRepaymentOfHouseholdIncomeFiancialDiaryRisk;
	}

	public String getLoanRepaymentOfHouseholdIncomeBudgetRisk() {
		return loanRepaymentOfHouseholdIncomeBudgetRisk;
	}

	public void setLoanRepaymentOfHouseholdIncomeBudgetRisk(String loanRepaymentOfHouseholdIncomeBudgetRisk) {
		this.loanRepaymentOfHouseholdIncomeBudgetRisk = loanRepaymentOfHouseholdIncomeBudgetRisk;
	}

	public String getDifferenceBetweenPresentAndExpectedHouseholdRisk() {
		return differenceBetweenPresentAndExpectedHouseholdRisk;
	}

	public void setDifferenceBetweenPresentAndExpectedHouseholdRisk(String differenceBetweenPresentAndExpectedHouseholdRisk) {
		this.differenceBetweenPresentAndExpectedHouseholdRisk = differenceBetweenPresentAndExpectedHouseholdRisk;
	}

	public String getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk() {
		return householdExpensesOfHouseholdIncomeFinancialDiaryRisk;
	}

	public void setHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk(String householdExpensesOfHouseholdIncomeFinancialDiaryRisk) {
		this.householdExpensesOfHouseholdIncomeFinancialDiaryRisk = householdExpensesOfHouseholdIncomeFinancialDiaryRisk;
	}

	public String getHouseholdExpensesOfHouseholdIncomeBudgetRisk() {
		return householdExpensesOfHouseholdIncomeBudgetRisk;
	}

	public void setHouseholdExpensesOfHouseholdIncomeBudgetRisk(String householdExpensesOfHouseholdIncomeBudgetRisk) {
		this.householdExpensesOfHouseholdIncomeBudgetRisk = householdExpensesOfHouseholdIncomeBudgetRisk;
	}

	public double getDiaryHouseholdInAverage() {
		return diaryHouseholdInAverage;
	}

	public void setDiaryHouseholdInAverage(double diaryHouseholdInAverage) {
		this.diaryHouseholdInAverage = diaryHouseholdInAverage;
	}

	public double getDiaryHouseholdOutAverage() {
		return diaryHouseholdOutAverage;
	}

	public void setDiaryHouseholdOutAverage(double diaryHouseholdOutAverage) {
		this.diaryHouseholdOutAverage = diaryHouseholdOutAverage;
	}

	public double getDiaryHouseholdNetAverage() {
		return diaryHouseholdNetAverage;
	}

	public void setDiaryHouseholdNetAverage(double diaryHouseholdNetAverage) {
		this.diaryHouseholdNetAverage = diaryHouseholdNetAverage;
	}

	public double getExpectedBusinessInAverage() {
		return expectedBusinessInAverage;
	}

	public void setExpectedBusinessInAverage(double expectedBusinessInAverage) {
		this.expectedBusinessInAverage = expectedBusinessInAverage;
	}

	public double getExpectedBusinessOutAverage() {
		return expectedBusinessOutAverage;
	}

	public void setExpectedBusinessOutAverage(double expectedBusinessOutAverage) {
		this.expectedBusinessOutAverage = expectedBusinessOutAverage;
	}

	public double getExpectedBusinessNetAverage() {
		return expectedBusinessNetAverage;
	}

	public void setExpectedBusinessNetAverage(double expectedBusinessNetAverage) {
		this.expectedBusinessNetAverage = expectedBusinessNetAverage;
	}

	public double getExpectedHouseholdInAverage() {
		return expectedHouseholdInAverage;
	}

	public void setExpectedHouseholdInAverage(double expectedHouseholdInAverage) {
		this.expectedHouseholdInAverage = expectedHouseholdInAverage;
	}

	public double getExpectedHouseholdOutAverage() {
		return expectedHouseholdOutAverage;
	}

	public void setExpectedHouseholdOutAverage(double expectedHouseholdOutAverage) {
		this.expectedHouseholdOutAverage = expectedHouseholdOutAverage;
	}

	public double getExpectedHouseholdNetAverage() {
		return expectedHouseholdNetAverage;
	}

	public void setExpectedHouseholdNetAverage(double expectedHouseholdNetAverage) {
		this.expectedHouseholdNetAverage = expectedHouseholdNetAverage;
	}

	public double getNetBusinessIncomeAsSalesRevenueFinancialDiary() {
		return netBusinessIncomeAsSalesRevenueFinancialDiary;
	}

	public void setNetBusinessIncomeAsSalesRevenueFinancialDiary(double netBusinessIncomeAsSalesRevenueFinancialDiary) {
		this.netBusinessIncomeAsSalesRevenueFinancialDiary = netBusinessIncomeAsSalesRevenueFinancialDiary;
	}

	public double getNetBusinessIncomeAsSalesRevenueBudget() {
		return netBusinessIncomeAsSalesRevenueBudget;
	}

	public void setNetBusinessIncomeAsSalesRevenueBudget(double netBusinessIncomeAsSalesRevenueBudget) {
		this.netBusinessIncomeAsSalesRevenueBudget = netBusinessIncomeAsSalesRevenueBudget;
	}

	public double getLoanRepaymentOfHouseholdIncomeFiancialDiary() {
		return loanRepaymentOfHouseholdIncomeFiancialDiary;
	}

	public void setLoanRepaymentOfHouseholdIncomeFiancialDiary(double loanRepaymentOfHouseholdIncomeFiancialDiary) {
		this.loanRepaymentOfHouseholdIncomeFiancialDiary = loanRepaymentOfHouseholdIncomeFiancialDiary;
	}

	public double getLoanRepaymentOfHouseholdIncomeBudget() {
		return loanRepaymentOfHouseholdIncomeBudget;
	}

	public void setLoanRepaymentOfHouseholdIncomeBudget(double loanRepaymentOfHouseholdIncomeBudget) {
		this.loanRepaymentOfHouseholdIncomeBudget = loanRepaymentOfHouseholdIncomeBudget;
	}

	public double getHouseholdExpensesOfHouseholdIncomeFinancialDiary() {
		return householdExpensesOfHouseholdIncomeFinancialDiary;
	}

	public void setHouseholdExpensesOfHouseholdIncomeFinancialDiary(double householdExpensesOfHouseholdIncomeFinancialDiary) {
		this.householdExpensesOfHouseholdIncomeFinancialDiary = householdExpensesOfHouseholdIncomeFinancialDiary;
	}

	public double getHouseholdExpensesOfHouseholdIncomeBudget() {
		return householdExpensesOfHouseholdIncomeBudget;
	}

	public void setHouseholdExpensesOfHouseholdIncomeBudget(double householdExpensesOfHouseholdIncomeBudget) {
		this.householdExpensesOfHouseholdIncomeBudget = householdExpensesOfHouseholdIncomeBudget;
	}

	public double getDifferenceBetweenPresentAndExpectedIncomeBusiness() {
		return differenceBetweenPresentAndExpectedIncomeBusiness;
	}

	public void setDifferenceBetweenPresentAndExpectedIncomeBusiness(double differenceBetweenPresentAndExpectedIncomeBusiness) {
		this.differenceBetweenPresentAndExpectedIncomeBusiness = differenceBetweenPresentAndExpectedIncomeBusiness;
	}

	public double getDifferenceBetweenPresentAndExpectedHousehold() {
		return differenceBetweenPresentAndExpectedHousehold;
	}

	public void setDifferenceBetweenPresentAndExpectedHousehold(double differenceBetweenPresentAndExpectedHousehold) {
		this.differenceBetweenPresentAndExpectedHousehold = differenceBetweenPresentAndExpectedHousehold;
	}

	public Boolean getAccepted() {
		return accepted;
	}

	public void setAccepted(Boolean accepted) {
		this.accepted = accepted;
	}

	/*public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}*/

	public boolean isUploaded() {
		return uploaded;
	}

	public void setUploaded(boolean uploaded) {
		this.uploaded = uploaded;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@Override
	public String toString() {
		return "Cashflow: Loan amount: " + getLoanAmount();
	}

	public String getMobileId() {
		return mobileId;
	}

	public void setMobileId(String mobileId) {
		this.mobileId = mobileId;
	}

}
