package com.spbd.rest.controller;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.model.UsersAndRoles;
import com.spbd.cashflow.spring.service.UserService;

@Controller
public class UserController {
	
	Logger logger = Logger.getLogger(UserController.class.getName());
	
	@Resource(name = "UserService")
	UserService userService;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public @ResponseBody List<User> getUsers() {
		List<User> userList = userService.getUsers();
		for(User user : userList) {
			user.setRoles(null);
			user.setApplicants(null);
		}
		return userList;
	}
	
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public @ResponseBody String addUser(@RequestBody User user) {
		userService.addUser(user);
		return "success";
	}
	
	@RequestMapping(value = "/user", method = RequestMethod.PUT)
	public @ResponseBody String updateUser(@RequestBody User user) {
		userService.updateUser(user);
		return "success";
	}
	
	@RequestMapping(value = "/user", method = RequestMethod.DELETE)
	public @ResponseBody String deleteUser(@RequestBody User user) {
		userService.deleteUser(user);
		return "success";
	}
	
	@RequestMapping(value = "/usersandroles", method = RequestMethod.POST)
	public @ResponseBody String addUserAndRoles(@RequestBody UsersAndRoles usersAndRoles) {
		userService.addUsersAndRoles(usersAndRoles);
		return "success";
	}
	
	@RequestMapping(value = "/usersandroles", method = RequestMethod.PUT)
	public @ResponseBody String updateUserAndRoles(@RequestBody UsersAndRoles usersAndRoles) {
		userService.updateUsersAndRoles(usersAndRoles);
		return "success";
	}
	
	@RequestMapping(value = "/usersandroles", method = RequestMethod.DELETE)
	public @ResponseBody String deleteUserAndRoles(@RequestBody UsersAndRoles usersAndRoles) {
		userService.deleteUsersAndRoles(usersAndRoles);
		return "success";
	}
	
	@RequestMapping(value = "/userapplicant", method = RequestMethod.POST)
	public @ResponseBody String addUserApplicant(@RequestBody User user, @RequestBody Applicant applicant) {
		userService.addUserApplicant(user,applicant);
		return "success";
	}
	
	@RequestMapping(value = "/user/id/{id}", method = RequestMethod.GET)
	public @ResponseBody User getUserById(@PathVariable("id") long id) {
		return c(userService.getUserById(id));
	}
	
	@RequestMapping(value = "/user/name/{username}", method = RequestMethod.GET)
	public @ResponseBody User getUserByUsername(@PathVariable("username") String username) {
		return c(userService.getUserByUserName(username));
	}
	
	@RequestMapping(value = "/user/email/{email}", method = RequestMethod.GET)
	public @ResponseBody User existEmail(@PathVariable("email") String email) {
		return c(userService.existEmail(email));
	}
	
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public @ResponseBody User getUpdateUserByUserName(@RequestParam("id") long id, @RequestParam("username") String username) {
		return c(userService.getUpdateUserByUserName(id,username));
	}
	
	@RequestMapping(value = "/user/email", method = RequestMethod.GET)
	public @ResponseBody User existUpdateEmail(@RequestParam("id") long id, @RequestParam("username") String username) {
		return c(userService.existUpdateEmail(id,username));
	}
	
	@RequestMapping(value = "/user/role/{id}", method = RequestMethod.GET)
	public @ResponseBody UsersAndRoles getUsersAndRolesById(@PathVariable("id") long id) {
		return userService.getUsersAndRolesById(id);
	}
	
	// lazy fetch
	public User c(User user) {
		user.setRoles(null);
		user.setApplicants(null);
		return user;
	}
	
}