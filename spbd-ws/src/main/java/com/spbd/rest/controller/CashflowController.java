package com.spbd.rest.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;
import com.spbd.cashflow.spring.model.Diary;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.service.ApplicantService;
import com.spbd.cashflow.spring.service.CashflowService;
import com.spbd.cashflow.spring.service.UserService;
import com.spbd.rest.model.CashflowJSON;
import com.spbd.rest.model.Employee;

@Controller
public class CashflowController {

	@Resource(name = "ApplicantService")
	ApplicantService applicantService;

	@Resource(name = "UserService")
	UserService userService;

	@Resource(name = "CashflowService")
	CashflowService cashflowService;

	@RequestMapping(value = "/cashflows", method = RequestMethod.GET)
	public @ResponseBody List<Cashflow> getCashflowList() {
		List<Cashflow> cashflows = cashflowService.getCashflows();
		for (Cashflow cashflow : cashflows){
			cashflow.setDiaries(null);
			cashflow.setApplicant(null);
		}
		return cashflows;
	}

	@RequestMapping(value = "/cashflows/id/{id}", method = RequestMethod.GET)
	public @ResponseBody Cashflow getCashflowById(@PathVariable("id") long id) {
		Cashflow cf = cashflowService.getCashflowById(id);
		cf.setDiaries(null);
		cf.setApplicant(null);
		return cf;
	}
	
	@RequestMapping(value = "/cashflows", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> addCasflows(
			@RequestParam("username") String username,
			@RequestParam("password") String password,
			@RequestBody List<CashflowJSON> cashflows) {
		try {
			// CHECK USER LOGIN
			User user = userService.getUserByUserName(username);
			if (user == null)
				return new ResponseEntity<String>("Incorrect Details",
						HttpStatus.UNAUTHORIZED);
			if (!(user.getUsername().equals(username) && user.getPassword()
					.equals(password)))
				return new ResponseEntity<String>("Incorrect Details",
						HttpStatus.UNAUTHORIZED);

			// Save ALL CASHFLOWS
			for (CashflowJSON cashflowJSON : cashflows) {
				try {
					Cashflow cashflow = convertToEntity(cashflowJSON);
					// if cashflow already exists, skip
					if (cashflow.getMobileId() != null
							&& cashflowService.getCashflowByMobileId(cashflow
									.getMobileId()) == null) {
						// if Applicant doesnt exist, ADD applicant to DB
						Applicant applicant = applicantService
								.getApplicantByName(cashflowJSON.getName(),
										cashflowJSON.getDateOfBirth());
						if (applicant == null) {
							applicant = new Applicant();
							applicant.setName(cashflowJSON.getName());
							applicant.setDob(cashflowJSON.getDateOfBirth());
							applicant.setLoanOfficer(user);
							applicantService.addApplicant(applicant);
							userService.addUserApplicant(user, applicant);
						}

						cashflow.setApplicant(applicant);
						// ADD CASHFLOW
						cashflowService.addCashflow(cashflow);
						// ADD CASHFLOW & APPLICATION RELATION
						applicantService.addApplicantCashflow(applicant,
								cashflow);
					}
				} catch (Exception e) {
					// Skip cashflow if exception
				}
			}

		} catch (Exception e) {
			return new ResponseEntity<String>("failed",
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>("success", HttpStatus.OK);
	}

	private static Cashflow convertToEntity(CashflowJSON cashflowJSON) {
		Cashflow cashflow = new Cashflow();

		cashflow.setMobileId(cashflowJSON.getMobileId());
		cashflow.setLoanAmount(cashflowJSON.getLoanAmount());

		cashflow.setDateSubmitted(cashflowJSON.getDateSubmitted());

		cashflow.setWeeklyRepayment(cashflowJSON.getWeeklyRepayment());

		cashflow.setWeeksToPay(cashflowJSON.getWeeksToPay());

		cashflow.setSummary(cashflowJSON.getSummary());

		cashflow.setAccepted(cashflowJSON.getAccepted());

		cashflow.setNumberOfDiaryMonths(cashflowJSON.getNumberOfDiaryMonths());

		cashflow.setDiaryBusinessInAverage(cashflowJSON
				.getDiaryBusinessInAverage());

		cashflow.setDiaryBusinessOutAverage(cashflowJSON
				.getDiaryBusinessOutAverage());

		cashflow.setDiaryBusinessNetAverage(cashflowJSON
				.getDiaryBusinessNetAverage());

		cashflow.setDiaryHouseholdInAverage(cashflowJSON
				.getDiaryHouseholdInAverage());

		cashflow.setDiaryHouseholdOutAverage(cashflowJSON
				.getDiaryHouseholdOutAverage());

		cashflow.setDiaryHouseholdNetAverage(cashflowJSON
				.getDiaryHouseholdNetAverage());

		cashflow.setExpectedBusinessInAverage(cashflowJSON
				.getExpectedBusinessInAverage());

		cashflow.setExpectedBusinessOutAverage(cashflowJSON
				.getExpectedBusinessOutAverage());

		cashflow.setExpectedBusinessNetAverage(cashflowJSON
				.getExpectedBusinessNetAverage());

		cashflow.setExpectedHouseholdInAverage(cashflowJSON
				.getExpectedHouseholdInAverage());

		cashflow.setExpectedHouseholdOutAverage(cashflowJSON
				.getExpectedHouseholdOutAverage());

		cashflow.setExpectedHouseholdNetAverage(cashflowJSON
				.getExpectedHouseholdNetAverage());
		// cashflow analysis 1

		cashflow.setNetBusinessIncomeAsSalesRevenueFinancialDiary(cashflowJSON
				.getNetBusinessIncomeAsSalesRevenueFinancialDiary());

		cashflow.setNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk(cashflowJSON
				.getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk());

		// cashflow analysis 2

		cashflow.setNetBusinessIncomeAsSalesRevenueBudget(cashflowJSON
				.getNetBusinessIncomeAsSalesRevenueBudget());

		cashflow.setNetBusinessIncomeAsSalesRevenueBudgetRisk(cashflowJSON
				.getNetBusinessIncomeAsSalesRevenueBudgetRisk());

		// cashflow analysis 3

		cashflow.setDifferenceBetweenPresentAndExpectedIncomeBusiness(cashflowJSON
				.getDifferenceBetweenPresentAndExpectedIncomeBusiness());

		cashflow.setDifferenceBetweenPresentAndExpectedIncomeBusinessRisk(cashflowJSON
				.getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk());
		// cashflow analysis 4

		cashflow.setLoanRepaymentOfHouseholdIncomeFiancialDiary(cashflowJSON
				.getLoanRepaymentOfHouseholdIncomeFiancialDiary());

		cashflow.setLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk(cashflowJSON
				.getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk());

		// cashflow analysis 5

		cashflow.setLoanRepaymentOfHouseholdIncomeBudget(cashflowJSON
				.getLoanRepaymentOfHouseholdIncomeBudget());

		cashflow.setLoanRepaymentOfHouseholdIncomeBudgetRisk(cashflowJSON
				.getLoanRepaymentOfHouseholdIncomeBudgetRisk());

		// cashflow analysis 6

		cashflow.setLoanRepaymentOfHouseholdIncomeBudgetRisk(cashflowJSON
				.getLoanRepaymentOfHouseholdIncomeBudgetRisk());

		cashflow.setDifferenceBetweenPresentAndExpectedHouseholdRisk(cashflowJSON
				.getDifferenceBetweenPresentAndExpectedHouseholdRisk());

		// cashflow analysis 7

		cashflow.setHouseholdExpensesOfHouseholdIncomeFinancialDiary(cashflowJSON
				.getHouseholdExpensesOfHouseholdIncomeFinancialDiary());

		cashflow.setHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk(cashflowJSON
				.getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk());

		// cashflow analysis 8

		cashflow.setHouseholdExpensesOfHouseholdIncomeBudget(cashflowJSON
				.getHouseholdExpensesOfHouseholdIncomeBudget());

		cashflow.setHouseholdExpensesOfHouseholdIncomeBudgetRisk(cashflowJSON
				.getHouseholdExpensesOfHouseholdIncomeBudgetRisk());

		cashflow.setDiaries(cashflowJSON.getDiaries());

		return cashflow;
	}

}
