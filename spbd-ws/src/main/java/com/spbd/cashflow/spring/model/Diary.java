package com.spbd.cashflow.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="Diary")
public class Diary implements Serializable {
	
	@Id
	@GeneratedValue
	@Column(name="id", unique = true)
	private long id;
	
	@Column(name="month_nr")
	private int monthNumber;
	
	@Column(name="business_in")
	private double businessIn;
	
	@Column(name="business_out")
	private double businessOut;
	
	@Column(name="household_in")
	private double householdIn;
	
	@Column(name="household_out")
	private double householdOut;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getMonthNumber() {
		return monthNumber;
	}

	public void setMonthNumber(int monthNumber) {
		this.monthNumber = monthNumber;
	}

	public double getBusinessIn() {
		return businessIn;
	}

	public void setBusinessIn(double businessIn) {
		this.businessIn = businessIn;
	}

	public double getBusinessOut() {
		return businessOut;
	}

	public void setBusinessOut(double businessOut) {
		this.businessOut = businessOut;
	}

	public double getHouseholdIn() {
		return householdIn;
	}

	public void setHouseholdIn(double householdIn) {
		this.householdIn = householdIn;
	}

	public double getHouseholdOut() {
		return householdOut;
	}

	public void setHouseholdOut(double householdOut) {
		this.householdOut = householdOut;
	}
	
	public String toString() {
		return "Diary: Month nr: " + getMonthNumber();
	}
}
