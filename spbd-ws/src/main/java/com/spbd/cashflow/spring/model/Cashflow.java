package com.spbd.cashflow.spring.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Cashflow")
public class Cashflow implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="id", unique = true)
	private long id;
	
	@Column(name="mobile_id")
	private String mobileId;
	
	@Column(name="loan_amount")
	private double loanAmount;
	
	@Column(name="date_submitted")
	private Date dateSubmitted;
	
	@Column(name="weekly_repayment")
	private double weeklyRepayment;
	
	@Column(name="weeks_to_pay")
	private int weeksToPay;
	
	@Column(name="summary")
	private String summary;
	
	@Column(name="accepted", columnDefinition = "BIT", length = 1)
	private Boolean accepted;
	
	@Column(name="number_of_diary_months")
	private int numberOfDiaryMonths;
	
	@Column(name="diary_business_in_average")
	private double diaryBusinessInAverage;
	
	@Column(name="diary_business_out_average")
	private double diaryBusinessOutAverage;
	
	@Column(name="diary_business_net_average")
	private double diaryBusinessNetAverage;
	
	@Column(name="diary_household_in_average")
	private double diaryHouseholdInAverage;
	
	@Column(name="diary_household_out_average")
	private double diaryHouseholdOutAverage;
	
	@Column(name="diary_household_net_average")
	private double diaryHouseholdNetAverage;
	
	@Column(name="expected_business_in_average")
	private double expectedBusinessInAverage;
	
	@Column(name="expected_business_out_average")
	private double expectedBusinessOutAverage;
	
	@Column(name="expected_business_net_average")
	private double expectedBusinessNetAverage;
	
	@Column(name="expected_household_in_average")
	private double expectedHouseholdInAverage;
	
	@Column(name="expected_household_out_average")
	private double expectedHouseholdOutAverage;
	
	@Column(name="expected_household_net_average")
	private double expectedHouseholdNetAverage;
	
	// cashflow analysis 1
	@Column(name="net_business_income_as_sales_revenue_financial_diary")
	private double netBusinessIncomeAsSalesRevenueFinancialDiary;
	
	@Column(name="net_business_income_as_sales_revenue_financial_diary_risk")
	private String netBusinessIncomeAsSalesRevenueFinancialDiaryRisk;
	
	// cashflow analysis 2
	@Column(name="net_business_income_as_sales_revenue_budget")
	private double netBusinessIncomeAsSalesRevenueBudget;
	
	@Column(name="net_business_income_as_sales_revenue_budget_risk")
	private String netBusinessIncomeAsSalesRevenueBudgetRisk;
	
	// cashflow analysis 3
	@Column(name="difference_between_present_and_expected_income_business")
	private double differenceBetweenPresentAndExpectedIncomeBusiness;
	
	@Column(name="difference_between_present_and_expected_income_business_risk")
	private String differenceBetweenPresentAndExpectedIncomeBusinessRisk;
	
	// cashflow analysis 4
	@Column(name="loan_repayment_of_household_income_financial_diary")
	private double loanRepaymentOfHouseholdIncomeFiancialDiary;
	
	@Column(name="loan_repayment_of_household_income_financial_diary_risk")
	private String loanRepaymentOfHouseholdIncomeFiancialDiaryRisk;
	
	// cashflow analysis 5
	@Column(name="loan_repayment_of_household_income_budget")
	private double loanRepaymentOfHouseholdIncomeBudget;
	
	@Column(name="loan_repayment_of_household_income_budget_risk")
	private String loanRepaymentOfHouseholdIncomeBudgetRisk;
	
	// cashflow analysis 6
	@Column(name="difference_between_present_and_expected_income_household")
	private double differenceBetweenPresentAndExpectedHousehold;
	
	@Column(name="difference_between_present_and_expected_income_household_risk")
	private String differenceBetweenPresentAndExpectedHouseholdRisk;
	
	// cashflow analysis 7
	@Column(name="household_expenses_of_household_income_financial_diary")
	private double householdExpensesOfHouseholdIncomeFinancialDiary;
	
	@Column(name="household_expenses_of_household_income_financial_diary_risk")
	private String householdExpensesOfHouseholdIncomeFinancialDiaryRisk;
	
	// cashflow analysis 8
	@Column(name="household_expenses_of_household_income_budget")
	private double householdExpensesOfHouseholdIncomeBudget;
	
	@Column(name="household_expenses_of_household_income_budget_risk")
	private String householdExpensesOfHouseholdIncomeBudgetRisk;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "Cashflow_Diary", joinColumns = {
        @JoinColumn(name = "cash_id")}, inverseJoinColumns = {
        @JoinColumn(name = "diary_id")})
	private List<Diary> diaries;
	
	@ManyToOne
	@JoinColumn(name="applicant")
	private Applicant applicant;
	

	public Cashflow() {
		diaries = new ArrayList<Diary>();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public List<Diary> getDiaries() {
		return diaries;
	}

	public void setDiaries(List<Diary> diaries) {
		this.diaries = diaries;
	}

	public double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}
	
	public Date getDateSubmitted() {
		return dateSubmitted;
	}

	public void setDateSubmitted(Date dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public double getWeeklyRepayment() {
		return weeklyRepayment;
	}

	public void setWeeklyRepayment(double weeklyRepayment) {
		this.weeklyRepayment = weeklyRepayment;
	}

	public int getWeeksToPay() {
		return weeksToPay;
	}

	public void setWeeksToPay(int weeksToPay) {
		this.weeksToPay = weeksToPay;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public int getNumberOfDiaryMonths() {
		return numberOfDiaryMonths;
	}

	public void setNumberOfDiaryMonths(int numberOfDiaryMonths) {
		this.numberOfDiaryMonths = numberOfDiaryMonths;
	}

	public double getDiaryBusinessInAverage() {
		return diaryBusinessInAverage;
	}

	public void setDiaryBusinessInAverage(double diaryBusinessInAverage) {
		this.diaryBusinessInAverage = diaryBusinessInAverage;
	}

	public double getDiaryBusinessOutAverage() {
		return diaryBusinessOutAverage;
	}

	public void setDiaryBusinessOutAverage(double diaryBusinessOutAverage) {
		this.diaryBusinessOutAverage = diaryBusinessOutAverage;
	}

	public double getDiaryBusinessNetAverage() {
		return diaryBusinessNetAverage;
	}

	public void setDiaryBusinessNetAverage(double diaryBusinessNetAverage) {
		this.diaryBusinessNetAverage = diaryBusinessNetAverage;
	}

	public String getNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk() {
		return netBusinessIncomeAsSalesRevenueFinancialDiaryRisk;
	}

	public void setNetBusinessIncomeAsSalesRevenueFinancialDiaryRisk(
			String netBusinessIncomeAsSalesRevenueFinancialDiaryRisk) {
		this.netBusinessIncomeAsSalesRevenueFinancialDiaryRisk = netBusinessIncomeAsSalesRevenueFinancialDiaryRisk;
	}

	public String getNetBusinessIncomeAsSalesRevenueBudgetRisk() {
		return netBusinessIncomeAsSalesRevenueBudgetRisk;
	}

	public void setNetBusinessIncomeAsSalesRevenueBudgetRisk(
			String netBusinessIncomeAsSalesRevenueBudgetRisk) {
		this.netBusinessIncomeAsSalesRevenueBudgetRisk = netBusinessIncomeAsSalesRevenueBudgetRisk;
	}

	public String getDifferenceBetweenPresentAndExpectedIncomeBusinessRisk() {
		return differenceBetweenPresentAndExpectedIncomeBusinessRisk;
	}

	public void setDifferenceBetweenPresentAndExpectedIncomeBusinessRisk(
			String differenceBetweenPresentAndExpectedIncomeBusinessRisk) {
		this.differenceBetweenPresentAndExpectedIncomeBusinessRisk = differenceBetweenPresentAndExpectedIncomeBusinessRisk;
	}

	public String getLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk() {
		return loanRepaymentOfHouseholdIncomeFiancialDiaryRisk;
	}

	public void setLoanRepaymentOfHouseholdIncomeFiancialDiaryRisk(
			String loanRepaymentOfHouseholdIncomeFiancialDiaryRisk) {
		this.loanRepaymentOfHouseholdIncomeFiancialDiaryRisk = loanRepaymentOfHouseholdIncomeFiancialDiaryRisk;
	}

	public String getLoanRepaymentOfHouseholdIncomeBudgetRisk() {
		return loanRepaymentOfHouseholdIncomeBudgetRisk;
	}

	public void setLoanRepaymentOfHouseholdIncomeBudgetRisk(
			String loanRepaymentOfHouseholdIncomeBudgetRisk) {
		this.loanRepaymentOfHouseholdIncomeBudgetRisk = loanRepaymentOfHouseholdIncomeBudgetRisk;
	}

	public String getDifferenceBetweenPresentAndExpectedHouseholdRisk() {
		return differenceBetweenPresentAndExpectedHouseholdRisk;
	}

	public void setDifferenceBetweenPresentAndExpectedHouseholdRisk(
			String differenceBetweenPresentAndExpectedHouseholdRisk) {
		this.differenceBetweenPresentAndExpectedHouseholdRisk = differenceBetweenPresentAndExpectedHouseholdRisk;
	}

	public String getHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk() {
		return householdExpensesOfHouseholdIncomeFinancialDiaryRisk;
	}

	public void setHouseholdExpensesOfHouseholdIncomeFinancialDiaryRisk(
			String householdExpensesOfHouseholdIncomeFinancialDiaryRisk) {
		this.householdExpensesOfHouseholdIncomeFinancialDiaryRisk = householdExpensesOfHouseholdIncomeFinancialDiaryRisk;
	}

	public String getHouseholdExpensesOfHouseholdIncomeBudgetRisk() {
		return householdExpensesOfHouseholdIncomeBudgetRisk;
	}

	public void setHouseholdExpensesOfHouseholdIncomeBudgetRisk(
			String householdExpensesOfHouseholdIncomeBudgetRisk) {
		this.householdExpensesOfHouseholdIncomeBudgetRisk = householdExpensesOfHouseholdIncomeBudgetRisk;
	}

	public double getDiaryHouseholdInAverage() {
		return diaryHouseholdInAverage;
	}

	public void setDiaryHouseholdInAverage(double diaryHouseholdInAverage) {
		this.diaryHouseholdInAverage = diaryHouseholdInAverage;
	}

	public double getDiaryHouseholdOutAverage() {
		return diaryHouseholdOutAverage;
	}

	public void setDiaryHouseholdOutAverage(double diaryHouseholdOutAverage) {
		this.diaryHouseholdOutAverage = diaryHouseholdOutAverage;
	}

	public double getDiaryHouseholdNetAverage() {
		return diaryHouseholdNetAverage;
	}

	public void setDiaryHouseholdNetAverage(double diaryHouseholdNetAverage) {
		this.diaryHouseholdNetAverage = diaryHouseholdNetAverage;
	}

	public double getExpectedBusinessInAverage() {
		return expectedBusinessInAverage;
	}

	public void setExpectedBusinessInAverage(double expectedBusinessInAverage) {
		this.expectedBusinessInAverage = expectedBusinessInAverage;
	}

	public double getExpectedBusinessOutAverage() {
		return expectedBusinessOutAverage;
	}

	public void setExpectedBusinessOutAverage(double expectedBusinessOutAverage) {
		this.expectedBusinessOutAverage = expectedBusinessOutAverage;
	}

	public double getExpectedBusinessNetAverage() {
		return expectedBusinessNetAverage;
	}

	public void setExpectedBusinessNetAverage(double expectedBusinessNetAverage) {
		this.expectedBusinessNetAverage = expectedBusinessNetAverage;
	}

	public double getExpectedHouseholdInAverage() {
		return expectedHouseholdInAverage;
	}

	public void setExpectedHouseholdInAverage(double expectedHouseholdInAverage) {
		this.expectedHouseholdInAverage = expectedHouseholdInAverage;
	}

	public double getExpectedHouseholdOutAverage() {
		return expectedHouseholdOutAverage;
	}

	public void setExpectedHouseholdOutAverage(double expectedHouseholdOutAverage) {
		this.expectedHouseholdOutAverage = expectedHouseholdOutAverage;
	}

	public double getExpectedHouseholdNetAverage() {
		return expectedHouseholdNetAverage;
	}

	public void setExpectedHouseholdNetAverage(double expectedHouseholdNetAverage) {
		this.expectedHouseholdNetAverage = expectedHouseholdNetAverage;
	}

	public double getNetBusinessIncomeAsSalesRevenueFinancialDiary() {
		return netBusinessIncomeAsSalesRevenueFinancialDiary;
	}

	public void setNetBusinessIncomeAsSalesRevenueFinancialDiary(
			double netBusinessIncomeAsSalesRevenueFinancialDiary) {
		this.netBusinessIncomeAsSalesRevenueFinancialDiary = netBusinessIncomeAsSalesRevenueFinancialDiary;
	}

	public double getNetBusinessIncomeAsSalesRevenueBudget() {
		return netBusinessIncomeAsSalesRevenueBudget;
	}

	public void setNetBusinessIncomeAsSalesRevenueBudget(
			double netBusinessIncomeAsSalesRevenueBudget) {
		this.netBusinessIncomeAsSalesRevenueBudget = netBusinessIncomeAsSalesRevenueBudget;
	}


	public double getLoanRepaymentOfHouseholdIncomeFiancialDiary() {
		return loanRepaymentOfHouseholdIncomeFiancialDiary;
	}

	public void setLoanRepaymentOfHouseholdIncomeFiancialDiary(
			double loanRepaymentOfHouseholdIncomeFiancialDiary) {
		this.loanRepaymentOfHouseholdIncomeFiancialDiary = loanRepaymentOfHouseholdIncomeFiancialDiary;
	}

	public double getLoanRepaymentOfHouseholdIncomeBudget() {
		return loanRepaymentOfHouseholdIncomeBudget;
	}

	public void setLoanRepaymentOfHouseholdIncomeBudget(
			double loanRepaymentOfHouseholdIncomeBudget) {
		this.loanRepaymentOfHouseholdIncomeBudget = loanRepaymentOfHouseholdIncomeBudget;
	}


	public double getHouseholdExpensesOfHouseholdIncomeFinancialDiary() {
		return householdExpensesOfHouseholdIncomeFinancialDiary;
	}

	public void setHouseholdExpensesOfHouseholdIncomeFinancialDiary(
			double householdExpensesOfHouseholdIncomeFinancialDiary) {
		this.householdExpensesOfHouseholdIncomeFinancialDiary = householdExpensesOfHouseholdIncomeFinancialDiary;
	}

	public double getHouseholdExpensesOfHouseholdIncomeBudget() {
		return householdExpensesOfHouseholdIncomeBudget;
	}

	public void setHouseholdExpensesOfHouseholdIncomeBudget(
			double householdExpensesOfHouseholdIncomeBudget) {
		this.householdExpensesOfHouseholdIncomeBudget = householdExpensesOfHouseholdIncomeBudget;
	}

	public double getDifferenceBetweenPresentAndExpectedIncomeBusiness() {
		return differenceBetweenPresentAndExpectedIncomeBusiness;
	}

	public void setDifferenceBetweenPresentAndExpectedIncomeBusiness(
			double differenceBetweenPresentAndExpectedIncomeBusiness) {
		this.differenceBetweenPresentAndExpectedIncomeBusiness = differenceBetweenPresentAndExpectedIncomeBusiness;
	}

	public double getDifferenceBetweenPresentAndExpectedHousehold() {
		return differenceBetweenPresentAndExpectedHousehold;
	}

	public void setDifferenceBetweenPresentAndExpectedHousehold(
			double differenceBetweenPresentAndExpectedHousehold) {
		this.differenceBetweenPresentAndExpectedHousehold = differenceBetweenPresentAndExpectedHousehold;
	}

	public Boolean getAccepted() {
		return accepted;
	}

	public void setAccepted(Boolean accepted) {
		this.accepted = accepted;
	}

	public String toString() {
		return "Cashflow: Loan amount: " + getLoanAmount();
	}

	public String getMobileId() {
		return mobileId;
	}

	public void setMobileId(String mobileId) {
		this.mobileId = mobileId;
	}
	
}
