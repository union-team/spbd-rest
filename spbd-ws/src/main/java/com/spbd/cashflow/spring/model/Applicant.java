package com.spbd.cashflow.spring.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Applicant")
public class Applicant implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="id", unique = true)
	private long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="dob")
	private Date dob;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "Applicant_Cashflow", joinColumns = {
        @JoinColumn(name = "app_id")}, inverseJoinColumns = {
        @JoinColumn(name = "cash_id")})
	private List<Cashflow> cashflows;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User loanOfficer;
	
	public Applicant() {
		cashflows = new ArrayList<Cashflow>();
	}

	public User getLoanOfficer() {
		return loanOfficer;
	}

	public void setLoanOfficer(User loanOfficer) {
		this.loanOfficer = loanOfficer;
	}

	public List<Cashflow> getCashflows() {
		return cashflows;
	}
	
	public void setCashflows(List<Cashflow> cashflows) {
		this.cashflows = cashflows;
	}

	public long getId() {
		return id;
	}

	public void setId(long app_id) {
		this.id = app_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return getName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Applicant other = (Applicant) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}	
	
}