package com.spbd.cashflow.spring.dao;
 
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;

@Repository
public class CashflowDAO  {
    @Autowired
    private SessionFactory sessionFactory;
 
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
 
    public long addCashflow(Cashflow cashflow) {
        getSessionFactory().getCurrentSession().save(cashflow);
        return cashflow.getId();
    }
   
    public void deleteCashflow(Cashflow cashflow) {
        getSessionFactory().getCurrentSession().delete(cashflow);
    }
   
    public void updateCashflow(Cashflow cashflow) {
        getSessionFactory().getCurrentSession().update(cashflow);
    }

    public List<Cashflow> getCashflows() {
        List<Cashflow> list = getSessionFactory().getCurrentSession().createQuery("from Cashflow").list();
        return list;
    	
    }
    
    public List<Cashflow> getCashflowsEager() {
        List list = getSessionFactory().getCurrentSession().createQuery("from Cashflow").list();
        List<Cashflow> newlist = new ArrayList<Cashflow>();
        for(int i = 0; i < list.size(); i++) {
        	Cashflow cf = (Cashflow) list.get(i);
            Hibernate.initialize(cf.getDiaries());
            newlist.add(cf);
        }
        return newlist;
    }
    
    public Cashflow getCashflowById(long id) {
        Cashflow cf = (Cashflow) getSessionFactory().getCurrentSession().get(Cashflow.class, id);
        Hibernate.initialize(cf.getDiaries());
        return cf;
    }
    
    public Cashflow getCashflowByMobileId(String mobId) {
	        return (Cashflow) getSessionFactory().getCurrentSession().createQuery("from Cashflow c where c.mobileId = '"+ mobId +"'" ).uniqueResult();
	}
 
}