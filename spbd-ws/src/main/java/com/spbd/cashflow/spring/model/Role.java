package com.spbd.cashflow.spring.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="Role")
public class Role {

	@Id
	@Column(name="role_id", unique = true)
	private int role_id;
	
	private String roleName;
	@ManyToMany(mappedBy = "roles")
	private List<User> users;
	
	//@Column(name="Role_name")
    //private String roleName;	

	//@Column(name="Active")
	//private boolean active;
	
	@Column(name="description")
	private String description;
	

	// Attributes Getters and Setters //
	
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}	
	
	public int getRole_id() {
		return role_id;
	}

	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/*public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}*/
	
	// End of the Getters and Setters //
	
}
