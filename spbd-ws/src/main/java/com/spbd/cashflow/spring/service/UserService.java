package com.spbd.cashflow.spring.service;
 
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spbd.cashflow.spring.dao.UserDAO;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.User;
import com.spbd.cashflow.spring.model.UsersAndRoles;


@Service("UserService")
@Transactional(readOnly = true)
public class UserService {
 
    @Autowired
    UserDAO UserDAO;

    @Transactional(readOnly = false)
    public void addUser(User user) {
        getUserDAO().addUser(user);
    }
    
    @Transactional(readOnly = false)
	public void addUsersAndRoles(UsersAndRoles userANDroles) {
		getUserDAO().addUsersAndRoles(userANDroles);
		
	} 

    @Transactional(readOnly = false)
    public void deleteUser(User user) {
        getUserDAO().deleteUser(user);
    }
 
    @Transactional(readOnly = false)
    public void deleteUsersAndRoles(UsersAndRoles userANDroles) {
    	getUserDAO().deleteUsersAndRoles(userANDroles);
    }

    @Transactional(readOnly = false)
    public void updateUser(User user) {
        getUserDAO().updateUser(user);
    }
    
    @Transactional(readOnly = false)
    public void updateUsersAndRoles(UsersAndRoles userANDroles) {
    	getUserDAO().updateUsersAndRoles(userANDroles);
      }

    @Transactional(readOnly = false)
    public void addUserApplicant(User user, Applicant applicant) {
    	getUserDAO().addUserApplicant(user, applicant);
    }

    public List<User> getUsers() {
        return getUserDAO().getUsers();
    }
    
    public User getUserById(long id) {
        return getUserDAO().getUserById(id);
    }
    
    public User getUserByUserName(String username) {
        return getUserDAO().getUserByUserName(username);
    }
    
    public User getUpdateUserByUserName(long id, String username) {
    	return getUserDAO().getUpdateUserByUserName(id, username);
    }
    
    public User existEmail(String email) {
        return getUserDAO().existEmail(email);
    }
    
    public User existUpdateEmail(long id, String email) {
    	return getUserDAO().existUpdateEmail(id,email);
    }
    
    public UsersAndRoles getUsersAndRolesById(long id) {
        return getUserDAO().getUsersAndRolesById(id);
    }    
 
    public UserDAO getUserDAO() {
        return UserDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
        this.UserDAO = userDAO;
    }


}