package com.spbd.cashflow.spring.service;
 
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spbd.cashflow.spring.dao.CashflowDAO;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;


@Service("CashflowService")
@Transactional(readOnly = true)
public class CashflowService {
 
    @Autowired
    CashflowDAO CashflowDAO;

    @Transactional(readOnly = false)
    public long addCashflow(Cashflow cashflow) {
        long id = getCashflowDAO().addCashflow(cashflow);
        return id;
    }
 

    @Transactional(readOnly = false)
    public void deleteCashflow(Cashflow cashflow) {
        getCashflowDAO().deleteCashflow(cashflow);
    }
 

    @Transactional(readOnly = false)
    public void updateCashflow(Cashflow cashflow) {
        getCashflowDAO().updateCashflow(cashflow);
    }

    public List<Cashflow> getCashflows() {
        return getCashflowDAO().getCashflows();
    }
 
    public List<Cashflow> getCashflowsEager() {
        return getCashflowDAO().getCashflowsEager();
    }
    
    public CashflowDAO getCashflowDAO() {
        return CashflowDAO;
    }

    public void setCashflowDAO(CashflowDAO cashflowDAO) {
        this.CashflowDAO = cashflowDAO;
    }
    
    public Cashflow getCashflowById(long id) {
        return getCashflowDAO().getCashflowById(id);
    }
    
    public Cashflow getCashflowByMobileId(String mobId)
    {
    	return getCashflowDAO().getCashflowByMobileId(mobId);
    }
}