package com.spbd.cashflow.spring.model;

public enum UserStatus {
	ACTIVE,
	INACTIVE;
}
