package com.spbd.cashflow.spring.service;
 
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spbd.cashflow.spring.dao.ApplicantDAO;
import com.spbd.cashflow.spring.model.Applicant;
import com.spbd.cashflow.spring.model.Cashflow;


@Service("ApplicantService")
@Transactional(readOnly = true)
public class ApplicantService {
 
    @Autowired
    ApplicantDAO applicantDAO;

    @Transactional(readOnly = false)
    public void addApplicant(Applicant applicant) {
        getApplicantDAO().addApplicant(applicant);
    }
 

    @Transactional(readOnly = false)
    public void deleteApplicant(Applicant applicant) {
        getApplicantDAO().deleteApplicant(applicant);
    }
 

    @Transactional(readOnly = false)
    public void updateApplicant(Applicant applicant) {
        getApplicantDAO().updateApplicant(applicant);
    }
    
    @Transactional(readOnly = false)
    public void addApplicantCashflow(Applicant applicant, Cashflow cashflow) {
    	getApplicantDAO().addApplicantCashflow(applicant,cashflow);
    }

    public List<Applicant> getApplicants() {
        return getApplicantDAO().getApplicants();
    }
    
    public Applicant getApplicantById(long id) {
        return getApplicantDAO().getApplicantById(id);
    }
    
    
    public Applicant getApplicantByName(String name,Date dob)
    {
    	return getApplicantDAO().getApplicantByName(name,dob);
    }
    
    public ApplicantDAO getApplicantDAO() {
        return applicantDAO;
    }

    public void setApplicantDAO(ApplicantDAO applicantDAO) {
        this.applicantDAO = applicantDAO;
    }
    
}